empty Folder. could not use https://pantheon.io/ as I am getting an error at setup

README.md created in repository describing your setup
Editor / IDE - NetBeans
Operating system - Installed on AWS EC2 with RDS backend (setup VPC, subnet, securitygroups, Elastic IP, network ACL etc) Server info - Windows server running IIS

2. Create 3 basic pages that use Panels in place editor. Name one page welcome
	created 3 pages using panel
	/welcome - 1 column structure appear on home page
	/page1	- 2 column structure with some dummy data
	/page3 	- 3 column structure with some dummy data

3. Create any custom content type with any fields
	created a basic page with dummy title and body
	/customcontent

4. Generate some content (nodes, and your custom content type)
	a. Hint: there are modules that do this 
	Thanks for the hint saved me a lot of work
	installed and enabled dummy_content tableform and token

5. Utilize views to dynamically generate a list of nodes that display in ascending publish date
	created TestView, Display: Page,, In database, Type: Content, Order by: Publish date ascending
	/testview

6. Create a search Facet based on Content Type
	installed and enabled a lot of modules based on instructions on https://www.drupal.org/node/1254698
	/search/node - then fowolled some instructions and created server and index and fianlly a view for the search facet

7. Create a custom module which:
	a. When a user logs in for the first time, redirects to a �welcome� page 
	b. The welcome page will display the user�s username
	- Created a basic page /customlogin (/welcome was already existing) - "Welcome page to display username" used global $user; and print l($user->name,'user/'.$user->uid); 
	After creating the basic page created a custom module MyModule

8. Create a sub theme
	a. Change the colors to kent state colors
	Created MySubtheme with a screenshot and logo
	Copied colors.css to MySubTheme from bartik. Colors modified in colors.css to match KSU colors